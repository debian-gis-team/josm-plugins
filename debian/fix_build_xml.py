#!/usr/bin/python

import sys
import shutil
import codecs
import xml.dom.minidom as minidom

def main():

    # Read the command line
    try:
        orig_filename = sys.argv[1]
        bak_filename = sys.argv[2]
    except IndexError:
        print >> sys.stderr, "Please specify XML filename and backup filename"

    # Parse the XML build.xml file
    doc = minidom.parse(orig_filename)

    # Remove Internet access in revision target
    for target in doc.getElementsByTagName('target'):
        if target.attributes['name'].value == 'revision':
            target.childNodes = []
    #revision_target.childNodes = [node for node in revision_target.childNodes
    #                              if node.nodeName != 'exec' and node.nodeName != 'delete']
    #[xmlproperty_node] = [node for node in revision_target.childNodes
    #                      if node.nodeName == 'xmlproperty']
    #xmlproperty_node.attributes['file'].value = 'svn-info.xml'

    # Make a backup copy of the build.xml file
    shutil.copyfile(orig_filename, bak_filename)
    with codecs.open(orig_filename, 'w', 'utf-8') as orig_file:
        doc.writexml(orig_file)

if __name__ == '__main__':
    main()
